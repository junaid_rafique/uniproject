<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $picture
 * @property string $address
 */

class EndUser extends Model
{
    public function vehicles(){
        return $this->hasMany(Vehicle::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function bookings(){
        return $this->hasMany(Booking::class);
    }

    public function ratings(){
        return $this->hasMany(Rating::class);
    }
}
