<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property int $endUser_id
 * @property int $service_id
 * @property int $ad_id
 * @property string $content
 */

class Comment extends Model
{
    public function endUser(){
        return $this->belongsTo(EndUser::class);
    }

    public function ad(){
        return $this->belongsTo(Ad::class);
    }
}
