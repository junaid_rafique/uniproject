<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MongoDB\BSON\Timestamp;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property int $package_id
 * @property int $workshop_id
 * @property string $description
 * @property Timestamp $duration
 * @property string $adType
 */

class Ad extends Model
{
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }

    public function workshop(){
        return $this->belongsTo(Workshop::class);
    }
}
