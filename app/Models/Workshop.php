<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property string $type
 * @property int $agent_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $picture
 * @property string $address
 */

class Workshop extends Model
{
    public function services(){
        return $this->hasMany(Service::class);
    }

    public function agent(){
        return $this->belongsTo(Agent::class);
    }
    public function bookings(){
        return $this->hasMany(Booking::class);
    }

    public function ads(){
        return $this->hasMany(Ad::class);
    }
    public function payment(){
        return $this->hasMany(Payment::class);
    }

}

