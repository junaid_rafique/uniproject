<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $picture
 */

class Admin extends Model
{
    //
}
