<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function workshop(){
        return $this->belongsTo(Workshop::class);
    }
    public function package(){
        return $this->belongsTo(Package::class);
    }

}
