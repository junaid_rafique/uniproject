<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property int $user_id
 * @property int $serial_num
 * @property string $color
 * @property string $model
 * @property string $year
 */

class Vehicle extends Model
{
    public function endUser(){
        return $this->belongsTo(EndUser::class);
    }
}
