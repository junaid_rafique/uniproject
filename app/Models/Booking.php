<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property boolean $status
 * @property dateTime $validTill
 * @property int $endUser_id
 */


class Booking extends Model
{
    public function endUser(){
        return $this->belongsTo(EndUser::class);
    }

    public function workshop(){
        return $this->belongsTo(Workshop::class);
    }
}
