<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property int $workshop_id
 * @property string $name
 * @property int $cost
 * @property string $description
 */

class Service extends Model
{
    public function workshop(){
        return $this->hasMany(Workshop::class);
    }

    public function ratings(){
        return $this->hasMany(Rating::class);
    }


}
