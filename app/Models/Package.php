<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property dateTime $duration
 * @property float $amount
 */

class Package extends Model
{
    public function ad(){
        return $this->belongsTo(Ad::class);
    }
    public function payment(){
        return $this->hasMany(Payment::class);
    }

}
