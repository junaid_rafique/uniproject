<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property int $endUser_id
 * @property int $service_id
 * @property string $starCount
 */

class Rating extends Model
{
    public function service(){
        return $this->belongsTo(Service::class);
    }

    public function endUser(){
        return $this->belongsTo(EndUser::class);
    }
}
