<?php

namespace App\Utils\Constants;


class AppConst
{
    const NO = 0;
    const YES = 1;

    const LIMIT = 20;
    const OFFSET = 0;

}