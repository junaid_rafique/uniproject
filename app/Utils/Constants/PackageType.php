<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 10/23/2018
 * Time: 1:00 PM
 */

namespace App\Utils\Constants;


class PackageType
{
    const Silver = 'Silver';
    const Gold = 'Gold';
    const Diamond = 'Diamond';


    public $pkgType = [
        self::Silver=>self::Silver,
        self::Gold=>self::Gold,
        self::Diamond=>self::Diamond
    ];
}