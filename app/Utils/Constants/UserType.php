<?php

namespace App\Utils\Constants;


/**
 * @property array $types
 *
 */
class UserType
{
    const ADMIN = 'Admin';
    const AGENT = 'Agent';
    const WORKSHOP = 'Workshop';
    const USER = 'EndUser';

    public $types = [
        self::ADMIN=>self::ADMIN,
        self::AGENT=>self::AGENT,
        self::USER=>self::USER,
        self::WORKSHOP=>self::WORKSHOP
    ];

}