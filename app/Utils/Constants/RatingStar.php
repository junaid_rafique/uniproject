<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 10/18/2018
 * Time: 11:27 AM
 */

namespace App\Utils\Constants;


class RatingStar
{
    const ONE = "OneStar";
    const TWO = "TwoStar";
    const THREE = "ThreeStar";
    const FOUR = "FourStar";
    const FIVE = "FiveStar";

    public $starCount = [
        self::ONE => self::ONE,
        self::TWO => self::TWO,
        self::THREE => self::THREE,
        self::FOUR => self::FOUR,
        self::FIVE => self::FIVE
    ];

}

