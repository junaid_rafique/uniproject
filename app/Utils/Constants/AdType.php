<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 10/18/2018
 * Time: 12:11 PM
 */

namespace App\Utils\Constants;


class AdType
{
    const AD = 0;
    const FEATURE = 1;

    public $adType =[
        self::AD => self::AD,
        self::FEATURE => self::FEATURE,
        ];
}