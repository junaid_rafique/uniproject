<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\Constants\UserType;
class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type' , array_values(UserType::$pkgTyp));
            $table->string('name',40);
            $table->string('email',40)->unique();
            $table->string('password', 24);
            $table->string('phone',15)->nullable();
            $table->string('picture',60)->nullable();
            $table->string('address',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
